var canvas = document.getElementById("screen");
var gfx    = canvas.getContext("2d");

// définition du constructeur du type Piece
var Piece = function(name, color, line, column){
    this.name    = name   || 'empty'; // si il n'y a pas de paramètre 'name', utiliser 'empty' 
    this.line    = line   || 0;
    this.column  = column || 0;
    this.color   = color  || 'grey';
    this.pieceId = undefined;
};

// définition d'une méthode du type Piece: il est crucial de la créer 
// au niveau du prototype et non pas de l'objet !
// Cette fonction sera pratique pour calculer le déplacement des pièces
// quelle que soit leur orientation
Piece.prototype.orientation = function() {
    return (this.color === 'white') ? +1 : -1;
}

// Constructeur du type Pawn, observez attentivement l'appel au constructeur de Piece !
// pieceId correspond aux coordonnées d'extraction des images du pion en blanc puis noir
var Pawn = function(color, line, column) {
    Piece.prototype.constructor.call(this, 'Pawn', color, line, column);
    this.pieceId = [[0, 5], [480, 5]];
}
Pawn.prototype = new Piece();

var Tower = function(color, line, column) {
    Piece.prototype.constructor.call(this, 'Tower', color, line, column);
    this.pieceId = [[240, 5], [480, 5]];
}
Tower.prototype = new Piece();

var createBoard = function(nbLine, nbColumn){ // avec des pieces vide
    var board = [];
    for(var lig = 0; lig<nbLine; lig++){
        board[lig] = [];
        for(var col = 0; col<nbColumn; col++){
            if(lig==0){
                if (col == 0)
                    board[lig][col] = new Tower('white',lig,col);
                if (col == 1)
                    board[lig][col] = new Tower('white',lig,col);
                if (col == 2)
                    board[lig][col] = new Tower('white',lig,col);
            }
            else if(lig==1){
                board[lig][col] = new Pawn('white',lig,col);
            }
            else if(lig==6){
                board[lig][col] = new Pawn('black',lig,col);
            }
            else if(lig==7){
                //noirs
            }
            else{
                board[lig][col] = new Piece(null,null,lig,col);
            }
        }
    }
    return board;
}
//var isEmpty = function(lig, col)
//var put = function(lig, col, piece)

var board = createBoard(8, 8);

var initBoard = function(){ // avec les pièces du jeu

}

var convertCoordinates = function(ligPixel, colPixel) {
    var lig = Math.ceil(ligPixel / (canvas.height/8)) - 1;
    var col = Math.ceil(colPixel / (canvas.width /8)) - 1;
    return [lig, col];
}

// liste des coordonnées des cellules libres accessibles par la pièce actuellement
// sélectionnée par l'utilisateur
var highlightedCells = [];

// on prend la coordonnée de la cellule à dessiner et une couleur correspondant 
// au carré dessiné dans la case si elle ne contient pas de pièce (gris si la 
// case n'est pas sélectionnée pour représenter un déplacement valide et jaune sinon) 
var drawCell = function(coord, color){
   var piece = board[coord[0]][coord[1]];
   var posPixX = piece.column*canvas.width/8;
   var posPixY = piece.line*canvas.height/8;


   if(piece.pieceId != null){
        //console.log("piece");
        if(piece.color=='white'){
            gfx.drawImage(chessSymbols,piece.pieceId[0][0],piece.pieceId[0][1],75,75,posPixX,posPixY,50,50);

            console.log("dessin piece en (" +posPixX+","+posPixY+")");
        }
        else if(piece.color=='black') {
            gfx.drawImage(chessSymbols,piece.pieceId[1][0],piece.pieceId[1][1],75,75,posPixX,posPixY,50,50);

            console.log("dessin piece en (" +posPixX+","+posPixY+")");
        }
    }
}

//var highlight = function(on)

canvas.addEventListener("mousedown", mouseClicked, false);
//var mouseClicked = function(event) { // Pourquoi cela ne fonctionne pas avec var ?!
function mouseClicked(event) {
/*    var ligCoord = event.pageY - canvas.offsetTop;
    var colCoord = event.pageX - canvas.offsetLeft;
    var coord    = convertCoordinates(ligCoord, colCoord);
    console.info(coord);
    if (highlightedCells.length > 0) {
        highlight(false);
        highlightedCells = [];
    }
    var piece = board[coord[0]][coord[1]];
    if (piece.name !== '.') {
        var moves = piece.getMoves();
        highlightedCells.push(moves);
        highlight(true);
    } else {
        // TODO: if empty and highlighted, move the piece !
        // doMove();
    }*/
}

// initialise le plateau en déposant les pièces de deux joueurs au début de la partie
initBoard();

// Pour dessiner le plateau, on spécifie le coin supérieur gauche, la 
// largeur et la hauteur. Dans cette fonction, on appelle drawCell 
// pour dessiner une cellule à une coordonnée donnée.
var drawGrid = function(x, y, width, height, nbLig, nbCol) {
    gfx.fillStyle='grey';
    gfx.fillRect(x,y,canvas.width,canvas.height);
    gfx.fillStyle='lightgrey';  
    var squareSize = [width/nbCol,height/nbLig];
    for(var i = 0; i<nbLig; i++){
        for(var j = 0; j<nbCol; j++){
            if(i%2 == 0 && j%2 == 1)
              gfx.fillRect(x+squareSize[0]*i,y+squareSize[1]*j,squareSize[0],squareSize[1]);
            if(i%2 == 1 && j%2 == 0)
                gfx.fillRect(x+squareSize[0]*i,y+squareSize[1]*j,squareSize[0],squareSize[1]);

        }
    }
    for(var i = 0; i<board.length; i++){
        for(var j = 0; j<board[i].length; j++){
            drawCell([i,j],'grey');
        }
    }
    //gfx.drawImage(chessSymbols,0,5,75,75,50,50,50,50);
}
